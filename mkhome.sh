#!/bin/sh

ln -s .bashrc ~/.bashrc
ln -s .bash_profile ~/.bash_profile
ln -s .clang-format ~/.clang-format
ln -s .git-completion.bash ~/.git-completion.bash
ln -s .git-prompt.sh ~/.git-prompt.sh
ln -s .gitconfig ~/.gitconfig
ln -s .gitignore ~/.gitignore
ln -s .gitignore_global ~/.gitignore_global
ln -s .inputrc ~/.inputrc
ln -s .screenrc ~/.screenrc
ln -s .tmux.conf ~/.tmux.conf
ln -s .vimrc ~/.vimrc

